import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final tabs = [
      "Pessoal",
      "Familiares",
      "Profissional",
      "Endereço",
      "Declaração de Vontade",
      "Evoluçōes"
    ];
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      home: DefaultTabController(
        length: tabs.length,
        child: Scaffold(
          appBar: AppBar(
            title: Text('Dados Pessoais'),
            automaticallyImplyLeading: false,
            backgroundColor: Colors.blue,
            //backgroundColor: Color(0xff5808e5),
            bottom: TabBar(
              indicatorColor: Colors.white,
              isScrollable: true,
              tabs: [
                for (final tab in tabs) Tab(text: tab),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              for (final tab in tabs)
                Center(
                  child: Text(tab, style: TextStyle(fontSize: 48)),
                ),
            ],
          ),
        ),
      ),
    );
  }
}